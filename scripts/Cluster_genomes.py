#!/usr/bin/env python

###############################################################################
#                                                                             #
#    Cluster_genomes                                                          #
#                                                                             #
#    A wrapper script for Cluster_genomes.pl and written for Docker for use   #
#    with Cyverse's Docker platform.                                          #
#                                                                             #
#    Copyright (C) Benjamin Bolduc                                            #
#                                                                             #
###############################################################################
#                                                                             #
#    This library is free software; you can redistribute it and/or            #
#    modify it under the terms of the GNU Lesser General Public               #
#    License as published by the Free Software Foundation; either             #
#    version 3.0 of the License, or (at your option) any later version.       #
#                                                                             #
#    This library is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU        #
#    Lesser General Public License for more details.                          #
#                                                                             #
#    You should have received a copy of the GNU Lesser General Public         #
#    License along with this library.                                         #
#                                                                             #
###############################################################################

__author__ = "Ben Bolduc"
__copyright__ = "Copyright 2016"
__credits__ = ["Ben Bolduc"]
__license__ = "LGPLv3"
__maintainer__ = "Ben Bolduc"
__email__ = "bolduc.10@osu.edu"
__status__ = "Development"

import sys
import os
import argparse
import subprocess
from pprint import pprint

parser = argparse.ArgumentParser(
    description='Clusters genomes',
    formatter_class=argparse.RawTextHelpFormatter)

options = parser.add_argument_group('Options')
options.add_argument('-d', '--source-dir', dest='source_dir', metavar='DIRECTORY')
options.add_argument('-f', '--fasta', dest='fasta_fn', metavar='DIRECTORY',
                     help='Filename of fasta file to cluster.')
options.add_argument('-c', '--coverage', dest='coverage', default=80,
                     help='% of the sequence covered.')
options.add_argument('-i', '--identity', dest='identity', default=95,
                     help='% of identity')
options.add_argument('-n', '--no-delete', dest='no_delete', action='store_true',
                     help='Do not delete files in mounted (source) directory')
options.add_argument('-l', '--log-file', dest='log_fn', metavar='FILENAME', default='Cluster_genomes.log',
                    help="Log file name")

results = parser.parse_args()


def error(msg):
    sys.stderr.write("ERROR: {}\n".format(msg))
    sys.stderr.flush()
    sys.exit(1)


def file_finder(rootdir, searchstring):
    found_list = []

    # found_list = [os.path.join(root, x) for x in files if searchstring in x]
    for root, dirs, files in os.walk(rootdir):
        for x in files:
            if searchstring in x:
                found_list.append(os.path.join(root, x))

    return found_list


def execute(command):

    print('Executing {}'.format(command))
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               shell=True)
    (stdout, stderr) = process.communicate()

    return stdout, stderr


if __name__ == '__main__':

    # Start logging
    log = open(results.log_fn, 'w')

    # Source directory - should mount source directory
    cwd = os.getcwd()

    # This next 'block' of code is the most overkill ever - and it's due to limitations in passing (or my inability)
    # filenames to a docker container within the Cyverse environment
    # Get name of file to be clustered
    fasta_fn = os.path.basename(results.fasta_fn)

    # Identify FULL PATH of file within Docker
    workable_file = file_finder(results.source_dir, fasta_fn)[0]

    # Remove all other files mounted, if enabled
    if not results.no_delete:  # If no delete isn't enabled (double negative = positive)
        for files in os.listdir(results.source_dir):
            if files != fasta_fn:
                os.remove(os.path.join(results.source_dir, files))

    identity = results.identity
    coverage = results.coverage
    cluster_cmd = 'Cluster_genomes.pl -f {} -c {} -i {}'.format(workable_file, coverage, identity)

    cluster_stdout, cluster_stderr = execute(cluster_cmd)

    pprint(cluster_stderr)

    pprint(cluster_stderr)

    log.write(cluster_stdout + os.sep)
    log.write(cluster_stderr + os.sep)

    log.close()