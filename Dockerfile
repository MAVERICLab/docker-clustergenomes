FROM ubuntu:14.04.3
MAINTAINER Benjamin Bolduc <bolduc.10@osu.edu>

# Remove those annoying warnings
ENV DEBIAN_FRONTEND noninteractive

ENV BINPATH /usr/bin

# build for MUMmer, wget for d/l, python for script (perl built-in)
RUN apt-get update && apt-get install -y \
    automake \
    build-essential \
    wget \
    python

RUN ldd /usr/local/bin/perl 

# Clean stuff up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV BITBUCKET_URL https://bitbucket.org/MAVERICLab/docker-clustergenomes/raw/7b6b8d1e75dc8eb6ac3a47f29c998a227382287c

RUN wget --no-verbose http://tenet.dl.sourceforge.net/project/mummer/mummer/3.23/MUMmer3.23.tar.gz
RUN tar xf MUMmer3.23.tar.gz && cd MUMmer3.23 && make && cp nucmer $BINPATH

RUN wget --no-verbose -P $BINPATH $BITBUCKET_URL/scripts/Cluster_genomes.pl
RUN wget --no-verbose -P $BINPATH $BITBUCKET_URL/scripts/Cluster_genomes.py

RUN chmod +x $BINPATH/Cluster_genomes.p*

RUN which Cluster_genomes.pl

ENTRYPOINT ["Cluster_genomes.py"]
CMD ["--help"]