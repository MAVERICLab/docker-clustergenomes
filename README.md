# Docker-ClusterGenomes

## Overview

**ClusterGenomes** is a perl script (wrapped in a tiny sliver of python) that makes sets of contigs/genomes non-redundant by clustering them, retaining only the seed for each cluster. Unlike other clustering tools, this can handle circular sequences). 

## Installation

Since everything runs within a Docker container, there is no need to install any dependencies, other than [Docker](https://www.docker.com/).

### Using the Dockerfile

Build the Docker image from the Dockerfile:

```
docker build -t maveric/clustergenomes:dev .
```

The "." in the code above means to use the Dockerfile from the current directory. (the file is literally named "Dockerfile")

Now you can "run" the Docker image - almost as if it's another program on your system.

```
docker run --rm -v $PWD/inputDir:inputDir -w /inputDir maveric/clustergenomes:dev --source-dir /inputDir --fasta contigs.fasta
```

Commands after the "maveric/clustergenomes:dev" are being passed to the Dockerized python script.

### Minimal command line options

**--rm**: Remove the Docker container after running.

**-v**: Bind directory from the *host* system to the container system. This is the **absolute host path**, followed by **:**, then where (on the *container's* system) to mount it.

**-w**: Sets working directory *within the container*.

**--source-dir** / **-d**: Tells the script *where* to look for FASTA input file. For ease of use, this **must be** the same directory that is mounted undre the container.

**--fasta** / **-f**: Name of the FASTA input file. It can be just the name *or* the path to the file. Due to limitations within the Docker environment of Cyverse, this allows users to select a file from within the above-mentioned directory to identify the fasta file at the exclusion of all other files. (This is opposed to trying to find the *right* fasta file if the directory already contains lots of fasta files anyway.)

### Other Options

**--log-file** / **-l**: Log file name (Default: Cluster_genomes.log).

**--coverage** / **-c**: Percent of sequence requiring coverage (Default: 80).

**--identity** / **-i**: Sequence identity required (Default: 95).

**--no-delete** / **-n**: Flag to prevent automatic deletion of files in the targeted directory (--dir, above). This was done in response to how Cyverse handles dockerizing directories.

### Test data

Test data was downloaded from the [Ocean Sampling Day 2014 assemblies](https://github.com/MicroB3-IS/osd-analysis/wiki/OSD-assemblies). [OSD46 contigs](https://owncloud.mpi-bremen.de/index.php/s/RDB4Jo0PAayg3qx?path=%2F2014%2Fassemblies%2FbySample%2FOSD46_2014-06-21_0m_NPL022).


### Authors

* Ben Bolduc (Authored python wrapper, Dockerized script and implemented as app in Cyverse)

* Simon Roux (Authored clustering script)